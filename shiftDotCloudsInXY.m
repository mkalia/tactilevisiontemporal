% Author : Megha Kalia
% Date : 12 Nov, 2021

% Change the location of stimulus in X ad Y direction

%Since the visual stimulus is generated randomly given a predetermined
%test stimulus location, the actual center of the 10 Gaussian blobs will 
%be different from the predetermined test location. This function moves the 
%whole collection of 10 blobs to align with the predetermined test location.

% centroid_predetermined is a 2x1 vector 

function locations = shiftDotCloudsInXY(currentLocations,centroid_predetermined,...
    ScreenInfo)
    %first initialize
    locations      = NaN(size(currentLocations));
    %get the actual center of the 10 Gaussian blobs
    averagedXY     = mean(currentLocations,2);
    centroid_xAxis = averagedXY(1);
    centroid_yAxis = averagedXY(2);
    
    %compute the difference
    diffX           = ScreenInfo.xmid + ScreenInfo.numPixels_perCM.*...
                        centroid_predetermined(1) - centroid_xAxis;
                    
    diffY           = ScreenInfo.ymid + ScreenInfo.numPixels_perCM.*...
        centroid_predetermined(2) - centroid_yAxis;
    %shifting the horizontal location of the blobs to match the
    %predtermined test location
    locations(1,:) = currentLocations(1,:) + diffX;
    locations(2,:) = currentLocations(2,:) + diffY;
end