% script checking for any press of "q" and closing everything before
% ending the calling script
% written by Stephanie Badde
% February 2016

% reset abort flag
abort = 0;

%check for queing characters
while CharAvail
    %read out the last character
    [responseTMP, ~]=GetChar;
    %check for quit command
    if strcmp(responseTMP,'q')
        %print info to console
        disp('User ended program');
        %raise abort counter
        abort = abort + 1;
        %only abort once
        if abort == 1
            try
                %clear the arduino connection
                delete(sBuzz)
            catch
            end
            try
                %clear the arduino connection
                clear arduResponse
            catch
            end
            try
                %close audio channel
                PsychPortAudio('Close', pahandle);
            catch
            end
            try
                %stop audio channel
                PsychPortAudio('Stop', pahandle2);
                %close audio channel
                PsychPortAudio('Close', pahandle2);
            catch
            end
            %clear the screen
            sca;
            returnFlag=1;
            fprintf('\n---------------------------------------------\n');
        end
    end
end
