%This script is a practice for the visual localization experiment.
%It doesn't include staircase procedure. We just use this to let
%participants to have an idea about what the experiment is like.
%% Enter subject's name and session number
%We first enter participants' ID number and session#.
%If this is the first session of the visual JND experiment, we will start
%by creating a file called "condition_sub.mat" that contains a randomized
%order of [2 10 16]. These values are the SD's of the gaussian distribution 
%where clouds are drawn from.
%If this is not their first session, we will find the file
%"condition_sub.mat" that we created before, and look for the SD value
%corresponding to the 2nd or the 3rd session.
clear all
close all
clc
% %enter subject's ID
% ExpInfo.subjID = [];
% while isempty(ExpInfo.subjID) == 1
%     try ExpInfo.subjID = input('Please enter participant ID#: ') ; %'s'
%     catch
%     end
% end
% %enter the session number
% ExpInfo.session = [];
% while isempty(ExpInfo.session) == 1
%     try ExpInfo.session = input('Please enter session#: ') ; %'s'
%     catch
%     end
% end
% 
% %determine the condition based on the input session#
% outFileName = ['Condition_sub' num2str(ExpInfo.subjID)];
% if ExpInfo.session == 1
%     Conditions_VisualJND = {Shuffle(Shuffle([2 10 16]))}; 
%     %[2 10 16] are the 3 possible SD of the gaussian distribution where the blobs are drawn from
%     save(outFileName,'Conditions_VisualJND');
%     ExpInfo.Condition = Conditions_VisualJND{1}(ExpInfo.session);
% else
%     temp = load(outFileName, 'Conditions_VisualJND');
%     ExpInfo.Condition = temp.Conditions_VisualJND{1}(ExpInfo.session);
% end
% out1FileName = ['JNDExperiment_visual_Practice_sub' num2str(ExpInfo.subjID) ...
%     '_session' num2str(ExpInfo.session)];

%% Screen Setup
%The struct ScreenInfo contains the size and the midpoints of the screen,
%the color of the background, and the unit that we use to tranform pixel to
%centimeter.
% Determine where your m-file's folder is.
% addpath(genpath('/Users/Guinevere/Documents/MATLAB/Psychtoolbox'))
Screen('Preference', 'VisualDebugLevel', 1);
Screen('Preference', 'SkipSyncTests', 1);
[windowPtr,rect] = Screen('OpenWindow', 1, [105,105,105]);
%[windowPtr,rect] = Screen('OpenWindow', 0, [105,105,105],[100 100 1000 480]); % for testing
[ScreenInfo.xaxis, ScreenInfo.yaxis] = Screen('WindowSize',windowPtr);
Screen('TextSize', windowPtr, 35) ;   
Screen('TextFont',windowPtr,'Times');
Screen('TextStyle',windowPtr,1); 

[center(1), center(2)] = RectCenter(rect);
ScreenInfo.xmid = center(1); % horizontal center
ScreenInfo.ymid = center(2); % vertical center
ScreenInfo.backgroundColor = 105;
ScreenInfo.numPixels_perCM = 7.5;
ScreenInfo.liftingYaxis = 300; 
%where do we want the centroid of clouds locate in y-axis (in pixel)

%% Define the visual stimuli
VSinfo.initialDistance = 23.3; 
%How far we want to present the first visual stimulus away from the center
%in centimeter (12.5 in visual angle)
% VSinfo.SD_yaxis = 10; 
VSinfo.SD_yaxis = 10; %Megha
%How big is the gaussian distribution in the vertical direction
%this value can be fixed because we only care about horizontal localization
% VSinfo.SD_blob = ExpInfo.Condition; 
VSinfo.SD_blob = 2; % Megha
%This is the SD of the gaussian distribution in the horizontal direction.
%This value is determined when we enter the subject session #
% VSinfo.num_randomDots = 10; 
VSinfo.num_randomDots = 5; %Megha

%number of dots

%create parameters for the gaussian white noise background
VSinfo.GWNnumPixel = 4; %4 pixels will have the same color
VSinfo.GWNnumFrames = 10; %we only generate 10 frames and use them repeatedly
% VSinfo.width = 401; 
VSinfo.width = 100; % Megha
%This is the width (SD) of the cloud. Increasing this value will make the
%cloud more blurry
VSinfo.boxSize = 201;
%This is the box size for each cloud.
VSinfo.intensity = 1;
%This determines the height of the clouds. Lowering this value will make
%them have lower contrast

%set the parameters for the visual stimuli, which consist of 10 gaussian blobs
VSinfo.greyScreen = ones(ScreenInfo.xaxis,ScreenInfo.yaxis).*...
    ScreenInfo.backgroundColor;
VSinfo.blankScreen = zeros(ScreenInfo.xaxis,ScreenInfo.yaxis);
x = 1:1:VSinfo.boxSize; y = x;
[X,Y] = meshgrid(x,y);
cloud = 1e2.*mvnpdf([X(:) Y(:)],[median(x) median(y)],[VSinfo.width 0;...
    0 VSinfo.width]);
VSinfo.Cloud = (255-ScreenInfo.backgroundColor).*VSinfo.intensity.*...
    reshape(cloud,length(x),length(y));  

%% Specify the experiment info
%creat a vector that contains the number of frames, which decrease from 10 to 3
ExpInfo.numFrames_easy = 17; %17
ExpInfo.numFrames_hard = 6;
ExpInfo.numFramesPostMasker =  60 - ExpInfo.numFrames_hard;
ExpInfo.numFrames_repetition = 5; %1 for testing, 5 for the real experiment
tempM = repmat(ExpInfo.numFrames_easy:-1:ExpInfo.numFrames_hard,...
    [ExpInfo.numFrames_repetition 1]);
ExpInfo.numFrames = tempM(:);
ExpInfo.numEasyTrials = length(ExpInfo.numFrames); %8 x 5 = 40 trials

%date_easyTrials stores all the data for randomly inserted easy trials
%1st row: the target will appear at either left or right side of the standard (-1: left; 1: right)
%2nd row: which appear first (1: the standard; 2: the target)
%3rd row: response (1: 1st interval; 2: second interval)
%4th row: participants think that the target is on the left or right of the standard location
%5th row: Response time
data_easyTrials = zeros(5,ExpInfo.numEasyTrials);
data_easyTrials(1,:) = Shuffle(Shuffle([ones(1,ExpInfo.numEasyTrials/2) -...
    1.*ones(1,ExpInfo.numEasyTrials/2)]));

Loc_coordinates = zeros(4*ExpInfo.numEasyTrials,VSinfo.num_randomDots); 
%This matriX store the coordinates of the actual visual stimuli
%1st row: standard location, x coordinates 
%2nd row: standard location, y coordinates
%3rd row: target location, x coordinates 
%4th row: target location, y coordinates 

RNGenerator = zeros(4*ExpInfo.numEasyTrials,VSinfo.num_randomDots); 
%Save all the generators just in case in the future we want to restore them  

%% Run the experiment by calling the function InterleavedStaircase
% %record movie
% moviePtr = Screen('CreateMovie',windowPtr,'VisualJNDExperiment.mov',...
%     ScreenInfo.xaxis,ScreenInfo.yaxis,60);
%start the experiment
% Screen('DrawText',windowPtr, 'Press any button to start the experiment.',...
% ScreenInfo.xmid-290,ScreenInfo.yaxis-ScreenInfo.liftingYaxis,[255 255 255]);
% Screen('Flip',windowPtr);
% KbWait(-3); %-1 if using the keyboard; -3 if using the attached keypad
% WaitSecs(1);

for i = 1:ExpInfo.numEasyTrials
    %display fixation cross for the first block
%     Screen('FillRect', windowPtr,[255 255 255], [ScreenInfo.xmid-7 ...
%         ScreenInfo.yaxis-ScreenInfo.liftingYaxis-1 ScreenInfo.xmid+7 ...
%         ScreenInfo.yaxis-ScreenInfo.liftingYaxis+1]);
%     Screen('FillRect', windowPtr,[255 255 255], [ScreenInfo.xmid-1 ...
%         ScreenInfo.yaxis-ScreenInfo.liftingYaxis-7 ScreenInfo.xmid+1 ...
%         ScreenInfo.yaxis-ScreenInfo.liftingYaxis+7]);
%     Screen('Flip',windowPtr);
    %Screen('AddFrameToMovie',windowPtr);
%     WaitSecs(1);
%     
%     Screen('Flip',windowPtr);
%     WaitSecs(2); %to make a fixation + blank screen 3 seconds in total
    
    %generate the post-masker first and pass it to the function, just to
    %make it faster
    gwn_background = generateNoisyBackground(VSinfo,ScreenInfo,windowPtr);

    %This function takes the position of the comparison stimuli (left or right), 
    %ExpInfo, VSinfo, ScreenInfo, post-masker and trial number as inputs, 
    %and generate response, RTs, the order of displaying the comparison and
    %the reference stimuli, locations of clouds, and RNGenerator as
    %outputs.
    
%     [data_easyTrials(3,i),data_easyTrials(4,i),data_easyTrials(5,i),...
%         data_easyTrials(2,i),Loc_coordinates((4*i-3):(4*i),:),...
%         RNGenerator((4*i-3):(4*i),:)] = DotStimuli(data_easyTrials(1,i),...
%         ExpInfo,VSinfo,ScreenInfo,gwn_background,windowPtr,i);
    [Loc_coordinates((4*i-3):(4*i),:),...
        RNGenerator((4*i-3):(4*i),:)] = DotStimuli(data_easyTrials(1,i),...
        ExpInfo,VSinfo,ScreenInfo,gwn_background,windowPtr,i);
end
%Screen('FinalizeMovie',moviePtr);

%% Save data and end the experiment
JNDExperiment_visual_Practice_data = {ScreenInfo,ExpInfo,VSinfo,data_easyTrials,...
    Loc_coordinates,RNGenerator};
save(out1FileName,'JNDExperiment_visual_Practice_data');
Screen('CloseAll');

