% Author : Megha Kalia
% Date : Nov 2, 2021ß

% Clear the workspace and the screen
sca;
close all;
clear;

% PsychDefaultSetup(2);
% rand('seed', sum(100 * clock));
% screens = Screen('Screens');
% screenNumber = max(screens);
% white = WhiteIndex(screenNumber);
% black = BlackIndex(screenNumber);
Screen('Preference', 'VisualDebugLevel', 1);
Screen('Preference', 'SkipSyncTests', 1);
[windowPtr,windowRect] = Screen('OpenWindow', 1, BlackIndex(1));
% [windowPtr, windowRect] = PsychImaging('OpenWindow', 1, BlackIndex(1));

[ScreenInfo.xaxis, ScreenInfo.yaxis] = Screen('WindowSize',windowPtr)
% [ScreenInfo.xaxis, ScreenInfo.yaxis] = Screen('WindowSize',windowPtr);

% [center(1), center(2)]     = RectCenter(rect);
% ScreenInfo.xmid            = center(1); % horizontal center
% ScreenInfo.ymid            = center(2); % vertical center
ScreenInfo.backgroundColor = 105;
ScreenInfo.numPixels_perCM = 7.5;
ScreenInfo.liftingYaxis    = 300; 

screenXpixels = ScreenInfo.xaxis;
screenYpixels = ScreenInfo.yaxis; 
% [screenXpixels, screenYpixels] = Screen('WindowSize', window);

% Get screen size in mm 
% [width, height] = Screen('DisplaySize', 1); % measure the screen size directly though

% Get the centre coordinate of the window in pixels
% For help see: help RectCenter
[xCenter, yCenter] = RectCenter(windowRect);
[center(1), center(2)] = RectCenter(windowRect);

%% screen set up 

% Screen('Preference', 'VisualDebugLevel', 1);
% Screen('Preference', 'SkipSyncTests', 1);
% [windowPtr,rect] = Screen('OpenWindow', 1, BlackIndex(1)); % original 0 
% %[windowPtr,rect] = Screen('OpenWindow', 0, [105,105,105],[100 100 1000 480]); % for testing
% [ScreenInfo.xaxis, ScreenInfo.yaxis] = Screen('WindowSize',windowPtr);
% % Screen('TextSize', windowPtr, 35) ;   
% % Screen('TextFont',windowPtr,'Times');
% % Screen('TextStyle',windowPtr,1); 
% 
% [center(1), center(2)] = RectCenter(rect);
% % ScreenInfo.xmid = center(1); % horizontal center
% % ScreenInfo.ymid = center(2); % vertical center
% ScreenInfo.backgroundColor = 105;
% ScreenInfo.numPixels_perCM = 7.5;
% ScreenInfo.liftingYaxis = 300; 

%%
% Enable alpha blending for anti-aliasing
% For help see: Screen BlendFunction?
% Also see: Chapter 6 of the OpenGL programming guide
% Screen('BlendFunction', windowPtr, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

% Use the meshgrid command to create our base dot coordinates. This will
% simply be a grid of equally spaced coordinates in the X and Y dimensions,
% centered on 0,0
% For help see: help meshgrid
% dim = 10;
% [x, y] = meshgrid(-dim:1:dim, -dim:1:dim);

% x_loc = 1:1.5:15;  % in mm 
% y_loc = ones(1, 10); 
x_loc = 1;  % in mm 
y_loc = 1; 

% Here we scale the grid so that it is in pixel coordinates. We just scale
% it by the screen size so that it will fit. This is simply a
% multiplication. Notive the "." before the multiplicaiton sign. This
% allows us to multiply each number in the matrix by the scaling value.
% pixelScale = screenYpixels / (dim * 2 + 2);

[width, height] = Screen('DisplaySize', 1); 
pixelScale = ScreenInfo.yaxis / height;
x_loc = x_loc .* pixelScale;
y_loc = y_loc .* pixelScale;
 
% Calculate the number of dots
% For help see: help numel
numDots = numel(x_loc);

xCenter = center(1); 
yCenter = center(2); 

% Make the matrix of positions for the dots. This need to be a two row
% vector. The top row will be the X coordinate of the dot and the bottom
% row the Y coordinate of the dot. Each column represents a single dot. For
% help see: help reshape
dotPositionMatrix = [reshape(x_loc, 1, numDots); reshape(y_loc, 1, numDots)];

dotPositionMatrix_adjusted = dotPositionMatrix; 
dotPositionMatrix_adjusted(1, :) = dotPositionMatrix_adjusted(1, :) + xCenter/4; 
dotPositionMatrix_adjusted(2, :) = dotPositionMatrix_adjusted(2, :) + yCenter; 


% We can define a center for the dot coordinates to be relaitive to. Here
% we set the centre to be the centre of the screen
% dotCenter = [xCenter yCenter];
dotCenter = [xCenter/4  yCenter];

% Set the color of our dot to be random i.e. a random number between 0 and
% 1
% dotColors = rand(3, numDots) .* white;
dotColors = [0 255 0];

% Set the size of the dots randomly between 10 and 30 pixels
% dotSizes = rand(1, numDots) .* 20 + 10;
dotSizes = 20; 

%% Blur the dots (borrowed from FangFang's code)

% Define the visual stimuli
VSinfo.initialDistance = 23.3; 
%How far we want to present the first visual stimulus away from the center
%in centimeter (12.5 in visual angle)
VSinfo.SD_yaxis = 10; 
%How big is the gaussian distribution in the vertical direction
%this value can be fixed because we only care about horizontal localization
% VSinfo.SD_blob = ExpInfo.Condition; 
VSinfo.SD_blob = 2; 
%This is the SD of the gaussian distribution in the horizontal direction.
%This value is determined when we enter the subject session #
VSinfo.num_randomDots = 1; 
%number of dots

%create parameters for the gaussian white noise background
VSinfo.GWNnumPixel = 4; %4 pixels will have the same color
VSinfo.GWNnumFrames = 10; %we only generate 10 frames and use them repeatedly
VSinfo.width = 401; 
%This is the width (SD) of the cloud. Increasing this value will make the
%cloud more blurry
VSinfo.boxSize = 201;
%This is the box size for each cloud.
VSinfo.intensity = 1;
%This determines the height of the clouds. Lowering this value will make
%them have lower contrast

%set the parameters for the visual stimuli, which consist of 10 gaussian blobs
VSinfo.greyScreen = ones(ScreenInfo.xaxis,ScreenInfo.yaxis).*...
    ScreenInfo.backgroundColor;
VSinfo.blankScreen = zeros(ScreenInfo.xaxis,ScreenInfo.yaxis);
x = 1:1:VSinfo.boxSize; y = x;
[X,Y] = meshgrid(x,y);
cloud = 1e2.*mvnpdf([X(:) Y(:)],[median(x) median(y)],[VSinfo.width 0;...
    0 VSinfo.width]);
VSinfo.Cloud = (255-ScreenInfo.backgroundColor).*VSinfo.intensity.*...
    reshape(cloud,length(x),length(y));  

dotClouds_standardLoc = generateDotClouds(windowPtr,...
                dotPositionMatrix_adjusted,VSinfo,ScreenInfo);

for j = 1:17 %6 frames (100 milliseconds)
    Screen('DrawTexture',windowPtr,dotClouds_standardLoc,[],...
        [0,0,ScreenInfo.xaxis,ScreenInfo.yaxis]);
    Screen('Flip',windowPtr);        
end 
        
% Screen('DrawTexture',windowPtr,dotClouds_standardLoc,[],...
%     [0,0,ScreenInfo.xaxis,ScreenInfo.yaxis]);
% Screen('Flip',windowPtr);

%%
% Draw all of our dots to the screen in a single line of code
% For help see: Screen DrawDots

% Screen('DrawDots', windowPtr, dotPositionMatrix,...
%     dotSizes, dotColors, dotCenter, 2);

% Flip to the screen. This command basically draws all of our previous
% commands onto the screen. See later demos in the animation section on more
% timing details. And how to demos in this section on how to draw multiple
% rects at once.
% For help see: Screen Flip?

% Screen('Flip', windowPtr);

% Now we have drawn to the screen we wait for a keyboard button press (any
% key) to terminate the demo. For help see: help KbStrokeWait
KbStrokeWait;

% Clear the screen. "sca" is short hand for "Screen CloseAll". This clears
% all features related to PTB. Note: we leave the variables in the
% workspace so you can have a look at them if you want.
% For help see: help sca
sca;