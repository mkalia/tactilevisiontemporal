% Author : Megha Kalia
% Date : 11-11-2021 
% Script to render 10 stimuli and then scale then based on two mouse click
% positions. There coudl also be a rotation involved though. SRT in 2D
clear all; 
close all; 
sca; 


%% set up visual stimuli 

Screen('Preference', 'VisualDebugLevel', 1);
Screen('Preference', 'SkipSyncTests', 1);
[windowPtr,rect] = Screen('OpenWindow', 1, [105,105,105]);
%[windowPtr,rect] = Screen('OpenWindow', 0, [105,105,105],[100 100 1000 480]); % for testing
[ScreenInfo.xaxis, ScreenInfo.yaxis] = Screen('WindowSize',windowPtr);
Screen('TextSize', windowPtr, 35) ;   
Screen('TextFont',windowPtr,'Times');
Screen('TextStyle',windowPtr,1); 

[center(1), center(2)] = RectCenter(rect);
ScreenInfo.xmid = center(1); % horizontal center
ScreenInfo.ymid = center(2); % vertical center
ScreenInfo.backgroundColor = 105;
% ScreenInfo.numPixels_perCM = 7.5;
ScreenInfo.numPixels_perCM = 36;% Megha
% ScreenInfo.liftingYaxis = 300; 
ScreenInfo.liftingYaxis = 450 % what is this 

%% Define the visual stimuli
% VSinfo.initialDistance = 23.3; 
VSinfo.initialDistance(1) = 1.5; % Megha % we are changing this later
VSinfo.initialDistance(2) = 0; % Megha

%How far we want to present the first visual stimulus away from the center
%in centimeter (12.5 in visual angle)
% VSinfo.SD_yaxis = 10; 
VSinfo.SD_yaxis = 0.2; %Megha
%How big is the gaussian distribution in the vertical direction
%this value can be fixed because we only care about horizontal localization
% VSinfo.SD_blob = ExpInfo.Condition; 
VSinfo.SD_blob = 0.2; % Megha
%This is the SD of the gaussian distribution in the horizontal direction.
%This value is determined when we enter the subject session #
% VSinfo.num_randomDots = 10; 
VSinfo.num_randomDots = 5; %Megha

%number of dots

%create parameters for the gaussian white noise background
VSinfo.GWNnumPixel = 4; %4 pixels will have the same color
VSinfo.GWNnumFrames = 10; %we only generate 10 frames and use them repeatedly
% VSinfo.width = 401; 
VSinfo.width = 15; % Megha
%This is the width (SD) of the cloud. Increasing this value will make the
%cloud more blurry
% VSinfo.boxSize = 201;
VSinfo.boxSize = 21; % Megha, should be an odd number otherwise error
%This is the box size for each cloud.
% VSinfo.intensity = 1;
VSinfo.intensity = 0.2; % Megha
%This determines the height of the clouds. Lowering this value will make
%them have lower contrast

%set the parameters for the visual stimuli, which consist of 10 gaussian blobs
VSinfo.greyScreen = ones(ScreenInfo.xaxis,ScreenInfo.yaxis).*...
    ScreenInfo.backgroundColor;
VSinfo.blankScreen = zeros(ScreenInfo.xaxis,ScreenInfo.yaxis);
x = 1:1:VSinfo.boxSize; y = x;
[X,Y] = meshgrid(x,y);
cloud = 1e2.*mvnpdf([X(:) Y(:)],[median(x) median(y)],[VSinfo.width 0;...
    0 VSinfo.width]);
VSinfo.Cloud = (255-ScreenInfo.backgroundColor).*VSinfo.intensity.*...
    reshape(cloud,length(x),length(y));  

%% Specify the experiment info
%creat a vector that contains the number of frames, which decrease from 10 to 3
ExpInfo.numFrames_easy = 17; %17
ExpInfo.numFrames_hard = 6;
ExpInfo.numFramesPostMasker =  60 - ExpInfo.numFrames_hard;
ExpInfo.numFrames_repetition = 5; %1 for testing, 5 for the real experiment
tempM = repmat(ExpInfo.numFrames_easy:-1:ExpInfo.numFrames_hard,...
    [ExpInfo.numFrames_repetition 1]);
ExpInfo.numFrames = tempM(:);
ExpInfo.numEasyTrials = length(ExpInfo.numFrames); %8 x 5 = 40 trials

%date_easyTrials stores all the data for randomly inserted easy trials
%1st row: the target will appear at either left or right side of the standard (-1: left; 1: right)
%2nd row: which appear first (1: the standard; 2: the target)
%3rd row: response (1: 1st interval; 2: second interval)
%4th row: participants think that the target is on the left or right of the standard location
%5th row: Response time
data_easyTrials = zeros(5,ExpInfo.numEasyTrials);
data_easyTrials(1,:) = Shuffle(Shuffle([ones(1,ExpInfo.numEasyTrials/2) -...
    1.*ones(1,ExpInfo.numEasyTrials/2)]));

Loc_coordinates = zeros(4*ExpInfo.numEasyTrials,VSinfo.num_randomDots); 
%This matriX store the coordinates of the actual visual stimuli
%1st row: standard location, x coordinates 
%2nd row: standard location, y coordinates
%3rd row: target location, x coordinates 
%4th row: target location, y coordinates 

RNGenerator = zeros(4*ExpInfo.numEasyTrials,VSinfo.num_randomDots); 
%Save all the generators just in case in the future we want to restore them  

%% noisy background
gwn_background = generateNoisyBackground(VSinfo,ScreenInfo,windowPtr);

%% display visual stimuli 
% Set the blend funciton for the screen
Final = zeros(ScreenInfo.xaxis,ScreenInfo.yaxis); 

% spread is from -6.0cm to 6.00cm from the center of the screen 
for i = 1:9
    
    VSinfo.initialDistance(1) = -6.0 + (i-1)*1.5; 
    VSinfo.initialDistance(2) = 0; 
%     [Loc_coordinates((4*i-3):(4*i),:),...
%         RNGenerator((4*i-3):(4*i),:)] = DotStimuli(data_easyTrials(1,i),...
%         ExpInfo,VSinfo,ScreenInfo,gwn_background,windowPtr,i);
    
    [Loc_coordinates((4*i-3):(4*i),:),...
        RNGenerator((4*i-3):(4*i),:), grayImage] = SimultaneousDotStimuli(1,...
        ExpInfo,VSinfo,ScreenInfo,gwn_background,windowPtr,i, 0, [0 0]);
    
    % add value of blank and grayimage together
    if(i==1)
        Final = grayImage; 
    else       
        Final(grayImage~=105) = grayImage(grayImage~=105); 
    end
end

%Turn the matrix to texture    
tex_finalCombined = Screen('MakeTexture', windowPtr, Final,[],[],[],2); 
    
% Display all the stimuli together
for j = 1:6 %6 frames (100 milliseconds)
            
    Screen('DrawTexture',windowPtr,tex_finalCombined,[],...
        [0,0,ScreenInfo.xaxis,ScreenInfo.yaxis]); %Megha: rectangular subpart of the screen where the texture should be drawn, what's the format
    Screen('Flip',windowPtr);      
end 
        
%% select two points on screen using mouse 
Screen('DrawText',windowPtr, 'click first of two points on screen',...
ScreenInfo.xmid-290,ScreenInfo.yaxis-ScreenInfo.liftingYaxis,[255 255 255]);
Screen('Flip',windowPtr);

[clicks,x1,y1,whichButton] = GetClicks(windowPtr,0)

Screen('DrawText',windowPtr, 'click second of two points on screen',...
ScreenInfo.xmid-290,ScreenInfo.yaxis-ScreenInfo.liftingYaxis,[255 255 255]);
Screen('Flip',windowPtr);

[clicks,x2,y2,whichButton] = GetClicks(windowPtr,0); 


% find the rotation 
PtsDstCoord_B = [x1,900-y1; x2,900-y2]; % flipping the y axis % seems to be working
% PtsDstCoord_B = [504, 450; 936, 450]; 
PtsSrcCoord_A = [504, 450; 936, 450]; 

X = inv(PtsSrcCoord_A) * PtsDstCoord_B; 

% convert all destination points (in pixels)
PtsSrcCoordAll_A = PtsSrcCoord_A; 

for i = 1:7
    PtsSrcCoordAll_A(end+1, :) = [504+i*54, 450]; 
end

%% convert all destination points (in cm) only for testing
% very complicated and dirty way of doing things

Final = zeros(ScreenInfo.xaxis,ScreenInfo.yaxis); 

% spread is from -6.0cm to 6.00cm from the center of the screen 
for i = 1:9
    
    Dst_point = PtsSrcCoordAll_A(i,:)*X; 
    VSinfo.initialDistance(1) = (Dst_point(1)-ScreenInfo.xmid)/ScreenInfo.numPixels_perCM; 
    
    VSinfo.initialDistance(2) = (Dst_point(2)-ScreenInfo.ymid)/ScreenInfo.numPixels_perCM; 
    
%     VSinfo.initialDistance = -6.0 + (i-1)*1.5; 
%     [Loc_coordinates((4*i-3):(4*i),:),...
%         RNGenerator((4*i-3):(4*i),:)] = DotStimuli(data_easyTrials(1,i),...
%         ExpInfo,VSinfo,ScreenInfo,gwn_background,windowPtr,i);
    
    [Loc_coordinates((4*i-3):(4*i),:),...
        RNGenerator((4*i-3):(4*i),:), grayImage] = SimultaneousDotStimuli(1,...
        ExpInfo,VSinfo,ScreenInfo,gwn_background,windowPtr,i, 1, Dst_point);
    
    % add value of blank and grayimage together
    if(i==1)
        Final = grayImage; 
    else       
        Final(grayImage~=105) = grayImage(grayImage~=105); 
    end
end

%Turn the matrix to texture    
tex_finalCombined = Screen('MakeTexture', windowPtr, Final,[],[],[],2); 
    
% Display all the stimuli together
for j = 1:6 %6 frames (100 milliseconds)
            
    Screen('DrawTexture',windowPtr,tex_finalCombined,[],...
        [0,0,ScreenInfo.xaxis,ScreenInfo.yaxis]); %Megha: rectangular subpart of the screen where the texture should be drawn, what's the format
    Screen('Flip',windowPtr);      
end 
       
% 
% VSinfo.initialDistance = 0; 


    
    
%% Display the cross in the middle, for sanity check 
% % Now we set the coordinates (these are all relative to zero we will let
% % the drawing routine center the cross in the center of our monitor for us)
% % Here we set the size of the arms of our fixation cross
% fixCrossDimPix = 40;
% xCoords = [-fixCrossDimPix fixCrossDimPix 0 0];
% yCoords = [0 0 -fixCrossDimPix fixCrossDimPix];
% allCoords = [xCoords; yCoords];
% 
% % Set the line width for our fixation cross
% lineWidthPix = 4;
% 
% % Draw the fixation cross in white, set it to the center of our screen and
% % set good quality antialiasing
% Screen('DrawLines', windowPtr, allCoords,...
%     lineWidthPix, [0 0 0], [ScreenInfo.xmid ScreenInfo.ymid]);
% 
% % Flip to the screen
% Screen('Flip', windowPtr);
% 
% % Wait for a key press
% KbStrokeWait; 

% display 10 of these stimuli 
    
    
    
