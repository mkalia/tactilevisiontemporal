Screen('Preference', 'VisualDebugLevel', 1);
Screen('Preference', 'SkipSyncTests', 1);
[windowPtr,rect] = Screen('OpenWindow', 1, [105,105,105]);
%[windowPtr,rect] = Screen('OpenWindow', 0, [105,105,105],[100 100 1000 480]); % for testing
[ScreenInfo.xaxis, ScreenInfo.yaxis] = Screen('WindowSize',windowPtr);
Screen('TextSize', windowPtr, 35) ;   
Screen('TextFont',windowPtr,'Times');
Screen('TextStyle',windowPtr,1); 

[center(1), center(2)] = RectCenter(rect);
ScreenInfo.xmid = center(1); % horizontal center
ScreenInfo.ymid = center(2); % vertical center
ScreenInfo.backgroundColor = 105;
ScreenInfo.numPixels_perCM = 7.5;
ScreenInfo.liftingYaxis = 300; 


%% Define the visual stimuli
VSinfo.initialDistance = 23.3; 
%How far we want to present the first visual stimulus away from the center
%in centimeter (12.5 in visual angle)
% VSinfo.SD_yaxis = 10; 
VSinfo.SD_yaxis = 10; %Megha
%How big is the gaussian distribution in the vertical direction
%this value can be fixed because we only care about horizontal localization
% VSinfo.SD_blob = ExpInfo.Condition; 
VSinfo.SD_blob = 2; % Megha
%This is the SD of the gaussian distribution in the horizontal direction.
%This value is determined when we enter the subject session #
% VSinfo.num_randomDots = 10; 
VSinfo.num_randomDots = 5; %Megha

%number of dots

%create parameters for the gaussian white noise background
VSinfo.GWNnumPixel = 4; %4 pixels will have the same color
VSinfo.GWNnumFrames = 10; %we only generate 10 frames and use them repeatedly
% VSinfo.width = 401; 
VSinfo.width = 100; % Megha
%This is the width (SD) of the cloud. Increasing this value will make the
%cloud more blurry
VSinfo.boxSize = 201;
%This is the box size for each cloud.
VSinfo.intensity = 1;
%This determines the height of the clouds. Lowering this value will make
%them have lower contrast

%set the parameters for the visual stimuli, which consist of 10 gaussian blobs
VSinfo.greyScreen = ones(ScreenInfo.xaxis,ScreenInfo.yaxis).*...
    ScreenInfo.backgroundColor;
VSinfo.blankScreen = zeros(ScreenInfo.xaxis,ScreenInfo.yaxis);
x = 1:1:VSinfo.boxSize; y = x;
[X,Y] = meshgrid(x,y);
cloud = 1e2.*mvnpdf([X(:) Y(:)],[median(x) median(y)],[VSinfo.width 0;...
    0 VSinfo.width]);
VSinfo.Cloud = (255-ScreenInfo.backgroundColor).*VSinfo.intensity.*...
    reshape(cloud,length(x),length(y));  

%% Specify the experiment info
%creat a vector that contains the number of frames, which decrease from 10 to 3
ExpInfo.numFrames_easy = 17; %17
ExpInfo.numFrames_hard = 6;
ExpInfo.numFramesPostMasker =  60 - ExpInfo.numFrames_hard;
ExpInfo.numFrames_repetition = 5; %1 for testing, 5 for the real experiment
tempM = repmat(ExpInfo.numFrames_easy:-1:ExpInfo.numFrames_hard,...
    [ExpInfo.numFrames_repetition 1]);
ExpInfo.numFrames = tempM(:);
ExpInfo.numEasyTrials = length(ExpInfo.numFrames); %8 x 5 = 40 trials

%date_easyTrials stores all the data for randomly inserted easy trials
%1st row: the target will appear at either left or right side of the standard (-1: left; 1: right)
%2nd row: which appear first (1: the standard; 2: the target)
%3rd row: response (1: 1st interval; 2: second interval)
%4th row: participants think that the target is on the left or right of the standard location
%5th row: Response time
data_easyTrials = zeros(5,ExpInfo.numEasyTrials);
data_easyTrials(1,:) = Shuffle(Shuffle([ones(1,ExpInfo.numEasyTrials/2) -...
    1.*ones(1,ExpInfo.numEasyTrials/2)]));

Loc_coordinates = zeros(4*ExpInfo.numEasyTrials,VSinfo.num_randomDots); 
%This matriX store the coordinates of the actual visual stimuli
%1st row: standard location, x coordinates 
%2nd row: standard location, y coordinates
%3rd row: target location, x coordinates 
%4th row: target location, y coordinates 

RNGenerator = zeros(4*ExpInfo.numEasyTrials,VSinfo.num_randomDots); 
%Save all the generators just in case in the future we want to restore them  

%% noisy background
gwn_background = generateNoisyBackground(VSinfo,ScreenInfo,windowPtr);
