%%%%Megha Kalia
%date : 10th Nov, 2021 
% Function to display the random gaussian dots. 



% function [updatedResponse,updatedLeftOrRight,updatedRTs,indicator,...
%     dots_coordinates,RNcoordinates] = DotStimuli(direction,ExpInfo,VSinfo,...
%     ScreenInfo,gwn_texture,windowPtr,trialNum)   
function [dots_coordinates,RNcoordinates] = DotStimuli(direction,ExpInfo,VSinfo,...
    ScreenInfo,gwn_texture,windowPtr,trialNum)   
    if nargin < 2; trialNum = 1; end

    %----------------------------------------------------------------------
    %-----------Calculate the coordinates of the target stimuli------------
    %----------------------------------------------------------------------   
    %compute the location of the test stimulus
    % Megha : we are changing only the position of stimulus in x direction,
    % to manipulate positions change VSinfor.initialDistance
    targetLoc = ScreenInfo.xmid +ScreenInfo.numPixels_perCM.*...
                VSinfo.initialDistance*direction; 
            
       while 1
        %randomly draw 10 (x,y) coordinates based on the centroid
        RNcoordinates = randn(4,VSinfo.num_randomDots);
        dots_standardLoc_coordinates = vertcat(ScreenInfo.xmid+...
            (ScreenInfo.numPixels_perCM.*VSinfo.SD_blob.*RNcoordinates(1,:)),...
            ScreenInfo.liftingYaxis + (ScreenInfo.numPixels_perCM.*...
            VSinfo.SD_yaxis.*RNcoordinates(2,:)));
        
        dots_targetLoc_coordinates = vertcat(targetLoc+(...
            ScreenInfo.numPixels_perCM.*VSinfo.SD_blob.*RNcoordinates(3,:)),...
            ScreenInfo.liftingYaxis+(ScreenInfo.numPixels_perCM.*...
            VSinfo.SD_yaxis.*RNcoordinates(4,:)));
        
        %make sure the center of the 10 blobs are aligned with the 
        %predetermined location of the test stimulus
        dots_standardLoc_coordinates_shifted = shiftDotClouds(...
            dots_standardLoc_coordinates,0,ScreenInfo);
        dots_targetLoc_coordinates_shifted = shiftDotClouds(...
            dots_targetLoc_coordinates,VSinfo.initialDistance*direction,ScreenInfo);
        
        %check if they are within the boundaries
        dots_coordinates = [dots_standardLoc_coordinates_shifted;...
            dots_targetLoc_coordinates_shifted];
        check_withinTheLimit = CheckWithinTheBoundaries(dots_coordinates,...
            VSinfo.boxSize,ScreenInfo);
        
        %if the generated dots are within boundaries, then pass the
        %coordinates to the function generateDotClouds that gives out the
        %image texture. 
        if check_withinTheLimit == 1
            dotClouds_standardLoc = generateDotClouds(windowPtr,...
                dots_standardLoc_coordinates_shifted,VSinfo,ScreenInfo);
            
            % Megha : We don't need this
            dotClouds_targetLoc = generateDotClouds(windowPtr,...
                dots_targetLoc_coordinates_shifted,VSinfo,ScreenInfo);
            break;
        end
    end

    %----------------------------------------------------------------------
    %---------------Display the target and comparision stimuli-------------
    %----------------------------------------------------------------------
    
        for j = 1:ExpInfo.numFrames(trialNum) %6 frames (100 milliseconds)
            Screen('DrawTexture',windowPtr,dotClouds_targetLoc,[],...
                [0,0,ScreenInfo.xaxis,ScreenInfo.yaxis]); %Megha: rectangular subpart of the screen where the texture should be drawn, what's the format
            
%             WaitSecs(0.010); 
            Screen('Flip',windowPtr);        
        end 
        
        % white noise on screen
%         for jj = 1:ExpInfo.numFramesPostMasker 
%             Screen('DrawTexture', windowPtr, gwn_texture(rem(jj,10)+1),[],...
%                 [0,0,ScreenInfo.xaxis,ScreenInfo.yaxis]); 
%             Screen('Flip',windowPtr);
%         end
        
        %show fixation cross for 1 s and then a blank screen for 2 s
%         Screen('FillRect', windowPtr,[255 255 255], [ScreenInfo.xmid-7 ...
%             ScreenInfo.yaxis-ScreenInfo.liftingYaxis-1 ScreenInfo.xmid+7 ...
%             ScreenInfo.yaxis-ScreenInfo.liftingYaxis+1]);
%         Screen('FillRect', windowPtr,[255 255 255], [ScreenInfo.xmid-1 ...
%             ScreenInfo.yaxis-ScreenInfo.liftingYaxis-7 ScreenInfo.xmid+1 ...
%             ScreenInfo.yaxis-ScreenInfo.liftingYaxis+7]);
%         Screen('Flip',windowPtr); WaitSecs(1);
%         Screen('Flip',windowPtr); WaitSecs(2); 
        
%         for j = 1:ExpInfo.numFrames(trialNum)
%             Screen('DrawTexture',windowPtr,dotClouds_targetLoc,[],...
%                 [0,0,ScreenInfo.xaxis,ScreenInfo.yaxis]);
%             Screen('Flip',windowPtr);
%         end
%         
%         for jj = 1:ExpInfo.numFramesPostMasker 
%             Screen('DrawTexture', windowPtr, gwn_texture(rem(jj,10)+1),[],...
%                 [0,0,ScreenInfo.xaxis,ScreenInfo.yaxis]); 
%             Screen('Flip',windowPtr);
%         end
        %black screen for 1 seconds
%         Screen('Flip',windowPtr); WaitSecs(1);
end