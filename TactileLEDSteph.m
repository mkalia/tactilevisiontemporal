% main script for tactile counting in the presence of visual stimuli at the
% same or different locations
% written by Stephanie Badde 
% NYU, August 2016 
 
%% General Setup 
% Clear the workspace and but not the screen
 instrreset 
cd '/users/kalia/Documents/MATLAB/TouchLight/MeghaExperiment'

clearvars -except windowPtr rect;

% name of the experiment
expName = 'expXXXIXa'; 

%initialize return flag 
returnFlag = 0;


% Here we call some default settings for setting up Psychtoolbox
PsychDefaultSetup(2);
AssertOpenGL;
 
% keyboard settings
KbName('UnifyKeyNames'); 

%erase keyboard queue 
FlushEvents;

%% initialize the tactile arduino
% arduResponse = arduino('Com8', 'Uno');%set analog input pins to pulldown
% //buttons
arduResponse = arduino('/dev/cu.usbmodem141101', 'Uno'); 
configurePin(arduResponse, 'A3', 'pullup');
configurePin(arduResponse, 'A4', 'pullup');
configurePin(arduResponse, 'A5', 'pullup');

%% Start up the buzzers and lights
% initialise serial object
% sBuzz = serial('Com7'); % Whatever port your USB is connected  to 
% sBuzz = serial('/dev/cu.usbmodem141201');%'/dev/cu.usbmodem14201'
% % % sBuzz.Baudrate=9600; % Same rate as def ined in Arduino 
% sBuzz.Baudrate=115200 ; % Same rate as defined in Arduino
% % % sBuzz.Baudrate=230400 ;
% % sBuzz.Baudrate=57600 ; 
% % 
% sBuzz.StopBits=1;
% sBuzz.Terminator='LF';
% sBuzz.Parity='none';
% sBuzz.FlowControl='none';
% 
% % open for usage, pause for process delay
%  fopen(sBuzz);  

 MeghaMarkStim('i','/dev/cu.usbmodem141201'); 
pause(2);


%% screen settings
%drop start screen
 Screen('Preference', 'VisualDebugLevel', 1);
% Skip sync test for debugging
Screen('Preference','SkipSyncTests',1);

% Get the screen numbers. This gives us a number for each of the screens
% attached to our computer.
screens = Screen('Screens');

% To draw we select the maximum of these numbers. So in a situation where we
% have two screens attached to our monitor we will draw to the external
% screen.
screenNumber = max(screens); 

% check if window is possibly already open
try
    %flip the back buffer to front
    Screen('FillRect',windowPtr, [0 0 0]);
    Screen('Flip',windowPtr);
catch
    % open window on projector screen
     [windowPtr,rect]=Screen('OpenWindow',screenNumber, 0);
end

% fill with black colour and flip the back buffer to front
Screen('FillRect',windowPtr, [0 0 0]);
Screen('Flip',windowPtr);

% set text formatting
Screen('TextSize',windowPtr, 28);

% get screen settings
screenWidth = rect(3);
screenHeight = rect(4);
screenCenterX = screenWidth/2;
screenCenterY = screenHeight/2;

% present everything to the left of the center on the x-axis
xPosition = screenCenterX * 0.95;
% present everything above the center on the y-axis
yPosition = screenCenterY * 1.25;


% draw question in the center of the screen
Screen('DrawText', windowPtr, '+', xPosition, yPosition, [200, 200, 200]);
% flip the back buffer to front
Screen('Flip',windowPtr);
% write information to terminal
disp('press any key to continue');
%wait for keypress to go on
 KbWait();
% flip the back buffer to front
Screen('Flip',windowPtr);

%% sound settings
% % frequence of all sounds and sound devices
% freq = 45000;
% % create white noise
% whiteNoise=randn(1,freq*10)*0.1;
% % initialize psychsound
% InitializePsychSound();
% % open audio channel for beeps
% pahandle=PsychPortAudio('Open', [], [], 0, freq, 1);
% 
% % fill buffer with white noise sound
% PsychPortAudio('FillBuffer', pahandle, whiteNoise);


%% Set up path to directory
%Initialize the path of this file:
%readout filename of this m-file
mainscriptFilename = mfilename;
%readout full path (inclusive file name) of this path
mainscriptFullFilename = mfilename('fullpath');
%get full path to the directory containing this m-file
mainscriptFullPath=mainscriptFullFilename(1:(end-length(mainscriptFilename)-1));
 (mainscriptFullPath)
%add scripts directory
addpath(pwd);
%change to scripts folder
% cd ..
%change to main folder
cd ..
%change to data direc tory
cd 'data'
cd(expName)
cd 'originals'
%add data directory
addpath(pwd);
%store path to data directory
dataDir = pwd;
% back to data folder
cd ..
%change to randomization directory
cd 'randomizations'
% add data directory
addpath(pwd);
% store path to data directory
randomizationsDir = pwd;


%% Input of subject + task information

% ask for subject number
subNo = input('Input the subject number:','s');%'6';
% bloc to start with
startBlocNo = input('Input the block number:'); %13;
%set stimulus duration in ms (x times detection threshold)
tactileDurationBase=ceil(2.5*26);


%% Define conditions
% store the differences between standard and test skin locations
tactileLocations = [-3, 0, 3];

% store the differences in vision positions
visualLocations = [-3, 0, 3];

% combine skin locations and hand positions into overall locations
combinedLocations = combvec(tactileLocations, tactileLocations, visualLocations)';

% delete trials with identical tactile locations
combinedLocations = combinedLocations(combinedLocations(:,1)~=combinedLocations(:,2),:);

% all possible stimulus combinations, all possible combinations of tactile
% (column 1), tactile (column 2), and visual (column 3) combinations. A 4
% indicates absence of the stimulus
stimConds = [combinedLocations;[combinedLocations(:,1), 4*ones(size(combinedLocations,1),1), combinedLocations(:,3)];
    [combinedLocations(:,1), combinedLocations(:,2), 4*ones(size(combinedLocations,1),1)];
    [combinedLocations(:,1), 4*ones(size(combinedLocations,1),1), 4*ones(size(combinedLocations,1),1)]];

% number of conditions equals number of locations times same/different
numConditions = size(stimConds,1);

% Number of trials per condition.
trialsPerCondition = 20;

% repeat combined distances + modality  to aquire matrix containing all trials
trialConditions = repmat(stimConds, trialsPerCondition, 1);

% Get the size of the condition matrix and store the number of trials
[numTrials,~] = size(trialConditions);

% get the number of blocs
numBlocs = 24;

% set the number of trials per bloc
trialsPerBloc = numTrials/numBlocs;

% Randomize the conditions bloc by bloc
trialConditions = trialConditions(randperm(size(trialConditions,1)),:);
% Randomize the conditions bloc by bloc
for i = 1:trialsPerCondition
    trialConditions(1+(i-1)*numConditions:i*numConditions,:) = trialConditions(Shuffle(1+(i-1)*numConditions:i*numConditions),:);
end

%% convert data to .txt file
% write trial conditions to txt file (only if it does not already exist)
if ~exist([randomizationsDir '/randomization_' expName '_' subNo, '.txt'], 'file')
    dlmwrite([randomizationsDir '/randomization_' expName '_' subNo, '.txt'], trialConditions);
end

% read in randomization file (by doing so, randomization remains the same
% after restart) % sz: cool!
trialConditions = dlmread([randomizationsDir '/randomization_' expName '_' subNo, '.txt']);

% change working directory to data directory
cd(dataDir)

%% settings of the experiment
%distance between arm locations in steps (200 steps = one round = 0.8cm)
visualDistance = 15; 

%distance between buzzer locations in mm
tactileDistance = 15;

%randomly change duration
tactileDuration=tactileDurationBase + round(randn(1)*2);
visualDuration=tactileDuration;

%the delay send to the arduino to temporallly align tactile and visual stimuli
tactileDelayApplied = 0;
visualDelayApplied = 9;


%% set up visual stimulus 

SetupVisualStimuli % script


%% Data file setup
%name of data file to write to
dataFileName = [expName '_' subNo '.txt'];
%open pointer to data file (write results after each trial)
dataFilePointer = fopen(dataFileName, 'at');
%prepare column heads for output
colHeaders = {'subNo', ...
    'blocCounter', 'trialCounterPerBloc', 'trialCounter',...
    'tactileLocation1','tactileLocation2','visualLocation',...
    'response',...
    'tactileDistance','visualDistance',...
    'vibrationDuration', 'tactileDelayApplied', 'visualDelayApplied'};
%print headers to text file
fprintf(dataFilePointer,'%s %s %s %s %s %s %s %s %s %s %s %s %s\n', colHeaders{:});

%initialize trial counter
trialCounter = 0;

%% setup screenInfo and Experiment 
[ScreenInfo.xaxis, ScreenInfo.yaxis] = Screen('WindowSize',windowPtr);
[center(1), center(2)] = RectCenter(rect);
ScreenInfo.xmid = center(1); % horizontal center
ScreenInfo.ymid = center(2); % vertical center
ScreenInfo.backgroundColor = 105;
% ScreenInfo.numPixels_perCM = 7.5;
ScreenInfo.numPixels_perCM = 36;% Megha
% ScreenInfo.liftingYaxis = 300; 
ScreenInfo.liftingYaxis = 450 % what is this 

%% Specify the experiment info
%creat a vector that contains the number of frames, which decrease from 10 to 3
ExpInfo.numFrames_easy = 17; %17
ExpInfo.numFrames_hard = 6;
ExpInfo.numFramesPostMasker =  60 - ExpInfo.numFrames_hard;
ExpInfo.numFrames_repetition = 5; %1 for testing, 5 for the real experiment
tempM = repmat(ExpInfo.numFrames_easy:-1:ExpInfo.numFrames_hard,...
    [ExpInfo.numFrames_repetition 1]);
ExpInfo.numFrames = tempM(:);
ExpInfo.numEasyTrials = length(ExpInfo.numFrames); %8 x 5 = 40 trials

%date_easyTrials stores all the data for randomly inserted easy trials
%1st row: the target will appear at either left or right side of the standard (-1: left; 1: right)
%2nd row: which appear first (1: the standard; 2: the target)
%3rd row: response (1: 1st interval; 2: second interval)
%4th row: participants think that the target is on the left or right of the standard location
%5th row: Response time
data_easyTrials = zeros(5,ExpInfo.numEasyTrials);
data_easyTrials(1,:) = Shuffle(Shuffle([ones(1,ExpInfo.numEasyTrials/2) -...
    1.*ones(1,ExpInfo.numEasyTrials/2)]));

Loc_coordinates = zeros(4*ExpInfo.numEasyTrials,VSinfo.num_randomDots); 
%This matriX store the coordinates of the actual visual stimuli
%1st row: standard location, x coordinates 
%2nd row: standard location, y coordinates
%3rd row: target location, x coordinates 
%4th row: target location, y coordinates 

RNGenerator = zeros(4*ExpInfo.numEasyTrials,VSinfo.num_randomDots); 
%Save all the generators just in case in the future we want to restore them  

%% noisy background
gwn_background = generateNoisyBackground(VSinfo,ScreenInfo,windowPtr);

%% Define the visual stimuli
% VSinfo.initialDistance = 23.3; 
VSinfo.initialDistance(1) = 1.5; % Megha % we are changing this later
VSinfo.initialDistance(2) = 0; % Megha

%How far we want to present the first visual stimulus away from the center
%in centimeter (12.5 in visual angle)
% VSinfo.SD_yaxis = 10; 
VSinfo.SD_yaxis = 0.2; %Megha
%How big is the gaussian distribution in the vertical direction
%this value can be fixed because we only care about horizontal localization
% VSinfo.SD_blob = ExpInfo.Condition; 
VSinfo.SD_blob = 0.2; % Megha
%This is the SD of the gaussian distribution in the horizontal direction.
%This value is determined when we enter the subject session #
% VSinfo.num_randomDots = 10; 
VSinfo.num_randomDots = 5; %Megha

%number of dots

%create parameters for the gaussian white noise background
VSinfo.GWNnumPixel = 4; %4 pixels will have the same color
VSinfo.GWNnumFrames = 10; %we only generate 10 frames and use them repeatedly
% VSinfo.width = 401; 
VSinfo.width = 15; % Megha
%This is the width (SD) of the cloud. Increasing this value will make the
%cloud more blurry
% VSinfo.boxSize = 201;
VSinfo.boxSize = 21; % Megha, should be an odd number otherwise error
%This is the box size for each cloud.
% VSinfo.intensity = 1;
VSinfo.intensity = 0.2; % Megha
%This determines the height of the clouds. Lowering this value will make
%them have lower contrast

%set the parameters for the visual stimuli, which consist of 10 gaussian blobs
VSinfo.greyScreen = ones(ScreenInfo.xaxis,ScreenInfo.yaxis).*...
    ScreenInfo.backgroundColor;
VSinfo.blankScreen = zeros(ScreenInfo.xaxis,ScreenInfo.yaxis);
x = 1:1:VSinfo.boxSize; y = x;
[X,Y] = meshgrid(x,y);
cloud = 1e2.*mvnpdf([X(:) Y(:)],[median(x) median(y)],[VSinfo.width 0;...
    0 VSinfo.width]);
VSinfo.Cloud = (255-ScreenInfo.backgroundColor).*VSinfo.intensity.*...
    reshape(cloud,length(x),length(y));  

%% iterate through the blocs
for blocCounter = startBlocNo:numBlocs
    
    % Calibrate 
    % light two LEDs at the extreme 
    Screen('DrawText', windowPtr, 'Press left mouse button to mark the position of first LED', xPosition*0.5, yPosition*0.6, [200, 200, 200]);
    %flip the back buffer to front
    Screen('Flip',windowPtr);
    MeghaMarkStim('z',0,0,1,2000,visualDuration+200,tactileDelayApplied,visualDelayApplied);  % LED lights for 2 seconds
    [clicks,x1,y1,whichButton] = GetClicks(windowPtr,0)
    WaitSecs(0.6);
%     % show both the LED and the mouse location. % feedback
%     Screen('DrawText', windowPtr, 'If you are happy with the point then press any key', xPosition*0.5, yPosition*0.6, [200, 200, 200]);
%     %flip the back buffer to front
%     Screen('Flip',windowPtr);
    
    
    % second button press 
    Screen('DrawText', windowPtr, 'Press left mouse button to mark the position of the second LED', xPosition*0.5, yPosition*0.6, [200, 200, 200]);
    %flip the back buffer to front
    Screen('Flip',windowPtr);
    MeghaMarkStim('z',0,0,7,2000,visualDuration+200,tactileDelayApplied,visualDelayApplied); % LED lights for 2 seconds
    
    [clicks,x2,y2,whichButton] = GetClicks(windowPtr,0); 
        
    % display stimuli 
    % find the rotation 
    PtsDstCoord_B = [x1,900-y1; x2,900-y2]; % flipping the y axis % seems to be working
    % PtsDstCoord_B = [504, 450; 936, 450]; 
    PtsSrcCoord_A = [504, 450; 936, 450]; 

    X = inv(PtsSrcCoord_A) * PtsDstCoord_B; 
 
    % convert all destination points (in pixels)
    PtsSrcCoordAll_A = PtsSrcCoord_A; 

    for i = 1:7
        PtsSrcCoordAll_A(end+1, :) = [504+i*54, 450]; 
    end

    %% convert all destination points (in cm) only for testing
    % very complicated and dirty way of doing things

    Final = zeros(ScreenInfo.xaxis,ScreenInfo.yaxis); 

    % spread is from -6.0cm to 6.00cm from the center of the screen 
    for i = 1:9

        Dst_point = PtsSrcCoordAll_A(i,:)*X; 
        VSinfo.initialDistance(1) = (Dst_point(1)-ScreenInfo.xmid)/ScreenInfo.numPixels_perCM; 

        VSinfo.initialDistance(2) = (Dst_point(2)-ScreenInfo.ymid)/ScreenInfo.numPixels_perCM; 

    %     VSinfo.initialDistance = -6.0 + (i-1)*1.5; 
    %     [Loc_coordinates((4*i-3):(4*i),:),...
    %         RNGenerator((4*i-3):(4*i),:)] = DotStimuli(data_easyTrials(1,i),...
    %         ExpInfo,VSinfo,ScreenInfo,gwn_background,windowPtr,i);

        [Loc_coordinates((4*i-3):(4*i),:),...
        RNGenerator((4*i-3):(4*i),:), grayImage] = SimultaneousDotStimuli(1,...
        ExpInfo,VSinfo,ScreenInfo,gwn_background,windowPtr,i, 1, Dst_point);
 
        % add value of blank and grayimage together
        if(i==1)
            Final = grayImage; 
        else       
            Final(grayImage~=105) = grayImage(grayImage~=105); 
        end
    end

    % display the visual scaled stimuli
    %Turn the matrix to texture    
    tex_finalCombined = Screen('MakeTexture', windowPtr, Final,[],[],[],2); 

    % Display all the stimuli together
    for j = 1:6 %6 frames (100 milliseconds)

        Screen('DrawTexture',windowPtr,tex_finalCombined,[],...
            [0,0,ScreenInfo.xaxis,ScreenInfo.yaxis]); %Megha: rectangular subpart of the screen where the texture should be drawn, what's the format
        Screen('Flip',windowPtr);      
    end 
    
    WaitSecs(1);
    
    % verfiy by locating the stimulus and the LED position 
    
    
    %draw question in the center of the screen
    Screen('DrawText', windowPtr, 'Press left button when you are aligned', xPosition*0.5, yPosition*0.6, [200, 200, 200]);
    %draw question in the center of th e screen
    Screen('DrawText', windowPtr, '+', xPosition, yPosition, [200, 200, 200]);
    %flip the back buffer to front 
    Screen('Flip',windowPtr);
    
    %print message to console
    fprintf('\n---- press left response box button when aligned ----\n');
    %reset response buttons 
    button_left = 1;
    %loop until response queue is empty
    while button_left == 1
        % give the serial port a command for arduino to execute
         MeghaMarkStim('z',8,8,4,0,visualDuration+200,tactileDelayApplied,visualDelayApplied);   
        tic
        while toc < 2% sz: ah, this might not be precise
            % read out response button pin, (button pressed -> 0); take minimun to avoid
            % overwriting due to short button presses at readout
            % sz: could have a check about what this "short button presses"
            % mean
            
             button_left =  min(readDigitalPin(arduResponse, 'A3'), button_left); 
        end
        % call subfunction to check for abort command and execute it
        abortCheckAndProcedure;
        if returnFlag == 1;  return; end
    end
    
    % flip the back buffer to front
    Screen('Flip',windowPtr);
    
    % wait shortly before each new trial
    WaitSecs(0.6);
    % call subfunction to check for abort command and execute it
    abortCheckAndProcedure;
    if returnFlag == 1; return; end 
     
    % draw question in the center of the screen
    Screen('DrawText', windowPtr, ['Press left button to start bloc ', num2str(blocCounter) ' of ' num2str(numBlocs)], xPosition*0.5, yPosition, [200, 200, 200]);
    % flip the back buffer to front
    Screen('Flip',windowPtr); 
    %print message to console
    fprintf('\n---- press left response box button to go on ----\n');
    %reset response buttons
    button_left = 1;
    %loop until response queue is empty
    while button_left == 1
        %read out response button pin, (button pressed -> 0); take minimun to avoid
        %overwriting due to short button presses at readout
        button_left = min(readDigitalPin(arduResponse, 'A3'), button_left);
        %call subfunction to check for abort command and execute it
        abortCheckAndProcedure;
        if returnFlag == 1; return; end
    end
    %flip the back buffer to front
    Screen('Flip',windowPtr);
    
    %start white noise sound with endless repetitions
%     PsychPortAudio('Start', pahandle, 0, 0, 1); %Megha 
    
    %wait shortly before each new trial
    WaitSecs(2);
    
    %re-initialize variable to store which trials to repeat
    trialsToRepeat = zeros(trialsPerBloc, 1);
    
    %re-initialize variable to store how many trials to run in this bloc
    trialsToRun = trialsPerBloc;
    
    %re-initialze trialBlocCounter
    trialBlocCounter = 1;
    
    %iterate through the trials
    while trialBlocCounter <= trialsToRun
        
        %call subfunction to check for abort command and execute it
        abortCheckAndProcedure;
        if returnFlag == 1; return; end
        
        %check which trial to run
        if trialBlocCounter <= trialsPerBloc
            % calculate trial ID
            trialCounter = (blocCounter-1)*trialsPerBloc + trialBlocCounter;
        elseif trialBlocCounter <= trialsPerBloc + sum(trialsToRepeat ~= 0)
            % choose trial to repeat
            trialCounter = trialsToRepeat(trialBlocCounter-trialsPerBloc ...% sz: reset it as zero (this should be the No. trialPerBloc trial, i.e. the last trial in the block)
                );
        else
            warning('Redo the trial ID calculations!'); return;% cool variable control: you never know whether an impossible condition will really not occur
        end
        
        %% read out trial settings
        % Store tactile location
        % Megha : This is where we are giving the tactile location control
        % the tactile from here
        tactileLocation_1 = trialConditions(trialCounter, 1); % sz comment: read from folder randomization
        
        %store tactile location
        tactileLocation_2 = trialConditions(trialCounter, 2);
        
        %read out test distance of vision
        visualLocation = trialConditions(trialCounter, 3);
        
        %d raw question in the center of the screen 
        Screen('DrawText', windowPtr, '+', xPosition, yPosition, [200, 200, 200]);
        %flip the back buffer to front
        Screen('Flip',windowPtr);
        %leave blank screen on shortly
        WaitSecs(1);
               
        %flip the back buffer to front
        Screen('Flip',windowPtr);
        
        %randomly change duration
        tactileDuration=tactileDurationBase + round(randn(1)*2);
        visualDuration=tactileDuration;
        
        %% send codes to serial port for activation
         
        % Megha 
    %         // Read the incoming comma separated integers
    %     buzzer1No = Serial.parseInt() - 1;
    %     buzzer2No = Serial.parseInt() - 1;
    %     ledNo = Serial.parseInt() - 1;
    %     buzzDuration  = Serial.parseInt();
    %     ledD uration = Serial.parseInt();  
    %     buzzDelay = Serial.parseInt();
    %     ledDelay = Serial.p a rseInt(); 
        % give the serial port a command for arduino to  execute
          
        % Megha: Give the stimuli display code here. 
%         [Loc_coordinates((4*i-3):(4*i),:),...
%         RNGenerator((4*i-3):(4*i),:)] = DotStimuli(data_easyTrials(1,i),... 
%         ExpInfo,VSinfo,ScreenInfo,gwn_background,windowPtr,i);
    
%         tStart = tic; 
        % Megha : delay in arduino's sti muli 
          
%          fprintf(sBuzz, '%d, %d, %d, %d, %d, %d, %d', [(tactileLocation_1+4) (tactileLocation_2+4) (visualLocation + 4) tactileDuration visualDuration 0 0]); % Megha
        tic
%         fprintf(sBuzz, '%d, %d, %d, %d, %d, %d, %d', [(tactileLocation_1+4) (tactileLocation_2+4) (visualLocation + 4) tactileDuration visualDuration tactileDelayApplied visualDelayApplied]);
         MeghaMarkStim('z',(tactileLocation_1+4), (tactileLocation_2+4), (visualLocation), 100, visualDuration, 0, 0); 
         MeghaMarkStim('t'); 
         k = MeghaMarkStim('m'); 
         if(strcmp(k, 'Set')) %HandShake
                     [Loc_coordinates((4*i-3):(4*i),:),...  
            RNGenerator((4*i-3):(4*i),:)] = DotStimuli(data_easyTrials( 1,i),...  
            ExpInfo,VSinfo,ScreenInfo,gwn_background,windowPtr,i);  
         end
        tEnd = toc 
        % calculate the delay in sending and receiving
%         WaitSecs(0.5 );
%          out = fscanf(sBuzz , '%d\n', 1); % 0.0023 
%          out = fread(sBuzz);    
           
%          [Loc_coordinates((4*i-3):(4*i),:),...  
%         RNGenerator((4*i-3):(4*i),:)] = DotStimuli(data_easyTrials( 1,i),...  
%         ExpInfo,VSinfo,ScreenInfo,gwn_background,windowPtr,i);  
         
%           onebyte = fread(sBuzz,1); % fread mentioning the command is faster // find a way to speed it up       
%           if (fread(sBuzz,1) == tactileDelayApplied) % change tis to t he first param //still delayed
%             [Loc_coordinates((4*i-3):(4*i),:),... 
%             RNGenerator((4*i-3):(4*i),:)] = DotStimuli(data_easyTrials( 1,i),... 
%             ExpInfo,VSinfo,ScreenInfo,gwn_background,windowPtr,i);
%           end 
           
         % with print+fread : 0.0023  
         
         % without print+fread : 0. 001
        
        % with write+fread : 0.0022
         % without write+fread : 9.8796e-04
         
%         tEnd = toc(tStart)   
%          out = fscanf(sBuzz, '%40s\n');  
%         signalArduino = strsplit (out, ','); % Seperate data based off commas 
% %         k=fread(sBuzz,20,'uchar');       
%             
%                 
%         if signalArduino{1} == 'y'
%             disp('we got the signal yay!\n'); 
%               
%             % Megha: Give the stimuli display code here. 
%             [Loc_coordinates((4*i-3):(4*i),:),...
%             RNGenerator((4*i-3):(4*i),:)] =
%             DotStimuli(data_easyTrials(1,i),... 111
%             ExpInfo,VSinfo,ScreenInfo,gwn_background,windowPtr,i); 
%     
%         end
        
          
                
        %wait very shortly 
        WaitSecs(1.5);
        
        %% collect response
        %reset satisfied
        satisfied = 0;
        
        %% collect response %%
        %draw question in the center of the screen
        Screen('DrawText', windowPtr, 'One (middle button) or two (right button) tactile stimuli?', xPosition*0.5, yPosition*0.9, [200, 200, 200]);
        %flip the back buffer to front
        Screen('Flip',windowPtr);
        %reset response buttons
        button_none = 1;
        button_one = 1;
        button_two = 1;
        %loop until response queue is empty 
        % sz: loop until any key is pressed
        while button_one + button_two + button_none== 3
            % read out response button pins, take minimun to avoid
            % overwriting due to short button presses at readout
            button_none = min(readDigitalPin(arduResponse, 'A3'), button_none);% sz: there is even a button for none...
            button_one = min(readDigitalPin(arduResponse, 'A4'), button_one);
            button_two = min(readDigitalPin(arduResponse, 'A5'), button_two);
            % call subfunction to check for abort command and execute it
            abortCheckAndProcedure;
            if returnFlag == 1; return; end
        end
        %assign response to button which was pressed, i.e. read out zero
        response=find([button_none button_one, button_two] == 0) - 1; % sz: transfer it from a 1-based index to 0-based
        
        %combine trial information
        trialInformation = {subNo, ...
            blocCounter, trialBlocCounter, trialCounter,...
            tactileLocation_1, tactileLocation_2, visualLocation, ...
            response,...
            tactileDistance, visualDistance,...
            tactileDuration, tactileDelayApplied, visualDelayApplied};
        %print trial information to text file
        fprintf(dataFilePointer,'%s %i %i %i %i %i %i %3.2f %i %i %1.3f %i %i\n', trialInformation{:});
        
        %display the information to experimentator %
        % sz: so there should betwo screen; BE CAREFUL ABOUT THE SCREE N
        % RESOLUTION AND DESIRED POSITION OF THE FIXATION CROSS!
        disp(trialInformation) 
        
        
        %raise trial bloc counter
        trialBlocCounter = trialBlocCounter + 1;
        
        %call subfunction to check for abort command and execute it
        abortCheckAndProcedure;
        if returnFlag == 1; return; end
        
    end
     
    %start white noise sound with endless repetitions
    PsychPortAudio('Stop', pahandle, 0, 0, 1);
    
end

%clear the arduino
%   delete(sBuzz)
  MeghaMarkStim('x');

%clear the arduino
clear arduResponse
